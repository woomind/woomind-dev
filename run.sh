#!/usr/bin/env bash

# Invoke the script from anywhere (e.g .bashrc alias)
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Colors
export BLUE="\033[94m"
export COLOR_ENDING="\033[0m"
export CHECK="\xE2\x9C\x94"
export CROSS="\xE2\x9D\x8C"

# Common variables
export COMMAND="$1"
export ERROR="You should either pass the 'start' or 'stop' argument."
export PROJECT='woomind'
export BASE="${PROJECT}-base"
export API="${PROJECT}-api"
export WEB="${PROJECT}-web"
export API_IMAGE="${PROJECT}:api"
export WEB_IMAGE="${PROJECT}:web"

COMPOSE=$(command -v docker-compose)
DOCKER=$(command -v docker)
KEYCHAIN=$(command -v keychain)
export COMPOSE
export DOCKER
export KEYCHAIN

function check_argument() {
  if [[ -z "${COMMAND}" ]]; then
    echo "${ERROR}"
    exit 1
  fi
}

function check_keychain() {
  echo -e "${BLUE}===> Checking keychain ${COLOR_ENDING}"
  # Confirm we have an existing SSH key loaded in the keychain.
  if [[ -f ${KEYCHAIN} ]]; then
    eval "$(keychain --eval id_rsa)"
  else
    echo "Sorry, this program cannot run if you don't have the keychain program installed. Exiting..."
    exit 0
  fi
}

function check_repo_freshness() {
  echo -e "${BLUE}===> Checking repo freshness ${COLOR_ENDING}"
  # Determine where to look for our repos.
  FILES="${DIR}/files"
  REPOS="${FILES}/${API} ${FILES}/${WEB}"

  for REPO in ${REPOS}; do
    # To prevent any oddity, confirm the current user owns all files.
    #
    # Using `ls -ld` is not optimal but great for portability.
    GIT_USER=$(ls -ld "${REPO}/.git" | awk '{print $3}')
    chown -R "${GIT_USER}:${GIT_USER}" "${REPO}/"

    # Clean up repos entirely before pulling changes.
    git -C "${REPO}" clean -fdx
    git -C "${REPO}" reset --hard
    git -C "${REPO}" pull

    if [ -z "$(git -C "${REPO}" status --porcelain)" ]; then
      echo -e "${CHECK} $(basename ${REPO}) repo is clean"
    else
      echo -e "${CROSS} ${REPO} repo is dirty"
    fi
  done
}

function run_app() {
  if [[ ${COMMAND} == 'start' ]]; then
    echo "===== Starting application ====="

    check_keychain
    check_repo_freshness

    ${COMPOSE} build --compress
    ${COMPOSE} up -d

    # Temporary fix because node.js is not reachable when the container starts
    # for the first time.
    sleep 2
    ${DOCKER} restart ${API}
    # End temporary fix

    # Now that the app is up, we can remove the base container
    ${DOCKER} rm -f ${BASE}

    # We're all set. Start tailing log files...
    ${DOCKER} logs -f ${API}

  elif [[ ${COMMAND} == 'stop' ]]; then
    echo "===== Stopping application ====="
    ${COMPOSE} down
    # Temporary fix
    ${DOCKER} rmi ${API_IMAGE} ${WEB_IMAGE}
    # End temporary fix
  else
    echo "${ERROR}"
    exit 1
  fi
}

check_argument
run_app
