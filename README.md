# woomind-dev

This is a WIP to create a set of containers to run the WooMind app.

## Prerequisites

### Install required software

Make sure both [Docker CE](https://docs.docker.com/install/) and [Docker Compose](https://github.com/docker/compose/releases) are installed.

```
$ docker --version && docker-compose --version
Docker version 18.09.0, build 4d60db4
docker-compose version 1.23.2, build 1110ad01
```

### Clone the requires repositories

* To clone the `woomind-dev` Git repo, follow instructions at https://github.com/anavarre/woomind-dev
* To clone the `woomind-api` Git repo, follow instructions at https://github.com/dclause/woomind-api
* To clone the `woomind-web` Git repo, follow instructions at https://github.com/dclause/woomind-web

### Prepare `woomind-dev`

From within the `/path/to/woomind-dev/files` directory, copy the `woomind-api` and `woomind-web` directories. The reason why you have to do so is because Docker doesn't allow for symlinks or files/directories outside the (Dockerfile) build context to be copied over containers.

The filesystem layout should look like below:

```
ls -l files/
total 16
-rw-r--r--  1 anavarre anavarre  873 janv. 30 13:18 api
-rw-rw-r--  1 anavarre anavarre  451 janv. 30 17:24 web
drwxrwxr-x 10 anavarre anavarre 4096 janv. 30 13:26 woomind-api
drwxrwxr-x  6 anavarre anavarre 4096 janv. 30 13:26 woomind-web
```

## Running the app

### Automatically

Simply run `./woomind start` from within the root of the repository. You may create a bash alias to invoke the command from anywhere.

### Manually

From within the `woomind-dev` repo, run:

```
$ docker-compose build
$ docker-composer up -d
```

This will:

* Check if the `mongo:latest` image exists. If not, build it.
* Check if the `woomind-db` container exists. If not, create it.
* Check if the `woomind:api` image exists. If not, build it.
* Check if the `woomind-api` container exists. If not, create it.
* Check if the `woomind:web` image exists. If not, build it.
* Check if the `woomind-web` container exists. If not, create it.

To connect to one of the WooMind containers, simply type:

```
$ docker exec -it <container-name> /bin/sh
```

#### Configure your `/etc/hosts` file

There's a handy script `hosts.sh` at the root of the repository. You can execute it to automatically add the correct API hostname to your local `/etc/hosts` file.

To confirm this is working, try and ping the host directly. E.g.

```
$ ping -c3 api.woomind.local
PING api.woomind.local (172.0.112.2) 56(84) bytes of data.
64 bytes from api.woomind.local (172.0.112.2): icmp_seq=1 ttl=64 time=0.102 ms
64 bytes from api.woomind.local (172.0.112.2): icmp_seq=2 ttl=64 time=0.095 ms
64 bytes from api.woomind.local (172.0.112.2): icmp_seq=3 ttl=64 time=0.093 ms
--- api.woomind.local ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2037ms
rtt min/avg/max/mdev = 0.093/0.096/0.102/0.012 ms
```

Note: `172.0.112.2` is a random IPv4 address. It'll change every time you spin up the WooMind containers, so don't rely on it ever.

## Populate the database with fake data

For querying the dataset, it might be useful to populate the datastore with fake data. Type the following to do so:

```
$ node seed
```

## Run tests

WooMind has full test coverage. To run tests from within the `woomind-api` container, simply type:

```
$ npm test
```

## Stopping the app

### Automatically

Simply run `./woomind stop` from within the root of the repository. You may create a bash alias to invoke the command from anywhere.

### Manually

From within the `woomind-dev` repo, run:

```
$ docker-compose down
```